import nldsl as nl
from nldsl.core import ExpressionRule, OperatorType
from . import docs

class CatCodeGenerator(nl.CodeGenerator):
    pass

class CatExpr(ExpressionRule):
    def __init__(self, expr_name, next_keyword):
        super().__init__(expr_name, next_keyword)
        # Create operator maps and types, e.g.:
        # self.operator_map['and'] = ' & '
        # self.operator_type['in'] = OperatorType.UNARY_FUNCTION
        self.operator_map['and'] = ' & '
        self.operator_map['or'] = ' | '
        self.operator_map['in'] = '.isin'
        self.operator_map['not'] = '~'
        self.operator_type['in'] = OperatorType.UNARY_FUNCTION

@nl.core.grammar(docs.EXPR_ONLY_DOC)
def func_expr_only(code, args):
    return code + args["expr"]

CatCodeGenerator.register_function(func_expr_only)

@nl.core.grammar(docs.TEST_DSL_DOC)
def func_test_dsl(code, args):
    return code + "print('Test', {0})".format(args["data"])

CatCodeGenerator.register_function(func_test_dsl)

@nl.core.grammar(docs.ON_CAT_DOC)
def func_on_cat(code, args):
    return code + "check({0})".format(args["cat"])

CatCodeGenerator.register_function(func_on_cat)

@nl.core.grammar(docs.CREATE_CAT_DOC)
def func_create_cat(code, args, env):
    return code + "Cat.createCat({0})".format(args["data"])

CatCodeGenerator.register_function(func_create_cat)

@nl.core.grammar(docs.LOAD_CAT_DOC)
def func_load_cat(code, args, env):
    return code + "Cat.read.format('{1}').load({0})".format(args["path"], args["type"])

CatCodeGenerator.register_function(func_load_cat)

@nl.core.grammar(docs.SAVE_TO_DOC)
def func_save_to(code, args):
    return code + ".write.format('{1}').save({0})".format(args["path"], args["type"])

CatCodeGenerator.register_function(func_save_to)

@nl.core.grammar(docs.FEED_DOC)
def func_feed(code, args, env):
    return code + ".Cat.feedCat({0})".format(args["food"])

CatCodeGenerator.register_function(func_feed)

CODE_GEN = CatCodeGenerator()

