EXPR_ONLY_DOC = ''' <Insert your function description here>
Example:
    Expression to be evaluated, e.g. x = z % (3 + a / p)
Grammar:
    !expr
Args:
    expr: <Insert useful argument description>
Type:
    Function
'''

TEST_DSL_DOC = ''' <Insert your function description here>
Example:
    x = test $data | transformer n ... | operation
Grammar:
    test $data
Args:
    data: <Insert useful argument description>
Type:
    Initialization
'''

ON_CAT_DOC = ''' <Insert your function description here>
Example:
    x = on $cat | transformer n ... | operation
Grammar:
    on $cat
Args:
    cat: <Insert useful argument description>
Type:
    Initialization
'''

CREATE_CAT_DOC = ''' <Insert your function description here>
Example:
    x = create cat from $data | transformer n ... | operation
Grammar:
    create cat from $data
Args:
    data: <Insert useful argument description>
Type:
    Initialization
'''

LOAD_CAT_DOC = ''' <Insert your function description here>
Example:
    x = load cat from $path as $type | transformer n ... | operation
Grammar:
    load cat from $path as $type
    type := {json,csv}
Args:
    path: <Insert useful argument description>
    type: <Insert useful argument description>
Type:
    Initialization
'''

SAVE_TO_DOC = ''' <Insert your function description here>
Example:
    on df | save to $path as $type
Grammar:
    save to $path as $type
    type := {json,csv}
Args:
    path: <Insert useful argument description>
    type: <Insert useful argument description>
Type:
    Operation
'''

FEED_DOC = ''' <Insert your function description here>
Example:
    on df | feed cat with $food
Grammar:
    feed cat with $food
    food := {tuna,chicken}
Args:
    food: <Insert useful argument description>
Type:
    Operation
'''


