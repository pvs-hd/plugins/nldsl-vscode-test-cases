import nldsl as nl
from nldsl.core import ExpressionRule, OperatorType
from . import docs

class Spark(nl.CodeGenerator):
    pass

class SparkExpr(ExpressionRule):
    def __init__(self, expr_name, next_keyword):
        super().__init__(expr_name, next_keyword)
        # Create operator maps and types, e.g.:
        # self.operator_map['and'] = ' & '
        # self.operator_type['in'] = OperatorType.UNARY_FUNCTION
        self.operator_map['and'] = ' & '
        self.operator_map['or'] = ' | '
        self.operator_map['in'] = '.isin'
        self.operator_map['not'] = '~'
        self.operator_type['in'] = OperatorType.UNARY_FUNCTION

@nl.core.grammar(docs.EXPR_ONLY_DOC)
def func_expr_only(code, args):
    return code + args["expr"]

Spark.register_function(func_expr_only)

@nl.core.grammar(docs.RE_TEST_DSL_DOC)
def func_re_test_dsl(code, args):
    return code + "print({0})".format(args["dataframe"])

Spark.register_function(func_re_test_dsl)

@nl.core.grammar(docs.ON_DATAFRAME_DOC)
def func_on_dataframe(code, args):
    return code + args["dataframe"]

Spark.register_function(func_on_dataframe)

@nl.core.grammar(docs.CREATE_DATAFRAME_DOC)
def func_create_dataframe(code, args, env):
    return code + "Spark.createDataFrame({}, schema={})".format(args["data"], args["header"])

Spark.register_function(func_create_dataframe)

@nl.core.grammar(docs.LOAD_FROM_DOC)
def func_load_from(code, args, env):
    return code + "Spark.read.format('{1}').load({0})".format(args["path"], args["type"])

Spark.register_function(func_load_from)

@nl.core.grammar(docs.SAVE_TO_DOC)
def func_save_to(code, args):
    return code + ".write.format('{1}').save({0})".format(args["path"], args["type"])

Spark.register_function(func_save_to)

@nl.core.grammar(docs.UNION_DOC)
def func_union(code, args):
    return code + ".unionByName({})".format(args["table"])

Spark.register_function(func_union)

@nl.core.grammar(docs.DIFFERENCE_DOC)
def func_difference(code, args):
    return code + ".subtract({})".format(args["table"])

Spark.register_function(func_difference)

@nl.core.grammar(docs.INTERSECTION_DOC)
def func_intersection(code, args):
    return code + ".intersect({})".format(args["table"])

Spark.register_function(func_intersection)

@nl.core.grammar(docs.SELECT_COLUMNS_DOC)
def func_select_columns(code, args):
    return code + ".select({})".format(args["columns"])

Spark.register_function(func_select_columns)

@nl.core.grammar(docs.SELECT_ROWS_DOC)
def func_select_rows(code, args):
    return code + ".filter({})".format(args["condition"])

Spark.register_function(func_select_rows)

@nl.core.grammar(docs.DROP_COLUMNS_DOC)
def func_drop_columns(code, args):
    return code + ".drop({})".format(args["columns"])

Spark.register_function(func_drop_columns)

@nl.core.grammar(docs.JOIN_DOC)
def func_join(code, args):
    return code + ".join({1}, on={2}, how='{0}')".format(args["how"], args["table"], args["columns"])

Spark.register_function(func_join)

@nl.core.grammar(docs.GROUP_BY_DOC)
def func_group_by(code, args):
    return code + ".groupBy({}).{}()".format(args["columns"], args["aggregation"])

Spark.register_function(func_group_by)

@nl.core.grammar(docs.REPLACE_VALUES_DOC)
def func_replace_values(code, args):
    return code + ".replace({}, {})".format(args["old_value"], args["new_value"])

Spark.register_function(func_replace_values)

@nl.core.grammar(docs.APPEND_DOC)
def func_append(code, args):
    cols_str = '.withColumn({})'
    return code + "".join(cols_str.format(', '.join([args['col_name'], args['col_expr']])))

Spark.register_function(func_append)

@nl.core.grammar(docs.SORT_BY_DOC)
def func_sort_by(code, args):
    cols, order = zip(*args['columns'])
    cols = nl.core.list_to_string(cols)
    order = [o == 'ascending' for o in order]
    return code + ".sort({}, ascending={})".format(cols, order)

Spark.register_function(func_sort_by)

@nl.core.grammar(docs.DROP_DUPLICATES_DOC)
def func_drop_duplicates(code):
    return code + ".dropDuplicates()"

Spark.register_function(func_drop_duplicates)

@nl.core.grammar(docs.RENAME_DOC)
def func_rename(code, args):
    ops = ['.withColumnRenamed({}, {})'.format(o, n) for o, n in args['columns']]
    return code + "".join(ops)

Spark.register_function(func_rename)

@nl.core.grammar(docs.SHOW_DOC)
def func_show(code):
    return code + ".show()"

Spark.register_function(func_show)

@nl.core.grammar(docs.DESCRIBE_DOC)
def func_describe(code):
    return code + ".describe()"

Spark.register_function(func_describe)

@nl.core.grammar(docs.COUNT_DOC)
def func_count(code):
    return code + ".count()"

Spark.register_function(func_count)

@nl.core.grammar(docs.SHOW_SCHEMA_DOC)
def func_show_schema(code):
    return code + ".printSchema()"

Spark.register_function(func_show_schema)

@nl.core.grammar(docs.HEAD_DOC)
def func_head(code, args):
    return code + ".head({})".format(args["num_rows"])

Spark.register_function(func_head)

@nl.core.grammar(docs.TEST_DSL_DOC)
def func_test_dsl(code, args):
    return code + "print({0})".format(args["dataframe"])

Spark.register_function(func_test_dsl)

CODE_GEN = Spark()

