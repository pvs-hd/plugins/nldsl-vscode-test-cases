EXPR_ONLY_DOC = ''' <Insert your function description here>
Example:
    Expression to be evaluated, e.g. x = z % (3 + a / p)
Grammar:
    !expr
Args:
    expr: <Insert useful argument description>
Type:
    Function
'''

RE_TEST_DSL_DOC = ''' <Insert your function description here>
Example:
    x = another test $dataframe | transformer n ... | operation
Grammar:
    another test $dataframe
Args:
    dataframe: <Insert useful argument description>
Type:
    Initialization
'''

ON_DATAFRAME_DOC = ''' <Insert your function description here>
Example:
    x = on $dataframe | transformer n ... | operation
Grammar:
    on $dataframe
Args:
    dataframe: <Insert useful argument description>
Type:
    Initialization
'''

CREATE_DATAFRAME_DOC = ''' <Insert your function description here>
Example:
    x = create dataframe from $data with header $header[$col_name] | transformer n ... | operation
Grammar:
    create dataframe from $data with header $header[$col_name]
Args:
    data: <Insert useful argument description>
    header: <Insert useful argument description>
Type:
    Initialization
'''

LOAD_FROM_DOC = ''' <Insert your function description here>
Example:
    x = test load from $path as $type | transformer n ... | operation
Grammar:
    test load from $path as $type
    type := {json,csv}
Args:
    path: <Insert useful argument description>
    type: <Insert useful argument description>
Type:
    Initialization
'''

SAVE_TO_DOC = ''' <Insert your function description here>
Example:
    on df | save to $path as $type
Grammar:
    save to $path as $type
    type := {json,csv}
Args:
    path: <Insert useful argument description>
    type: <Insert useful argument description>
Type:
    Operation
'''

UNION_DOC = ''' <Insert your function description here>
Example:
    x = on df | union $table
Grammar:
    union $table
Args:
    table: <Insert useful argument description>
Type:
    Transformation
'''

DIFFERENCE_DOC = ''' <Insert your function description here>
Example:
    x = on df | difference $table
Grammar:
    difference $table
Args:
    table: <Insert useful argument description>
Type:
    Transformation
'''

INTERSECTION_DOC = ''' <Insert your function description here>
Example:
    x = on df | intersection $table
Grammar:
    intersection $table
Args:
    table: <Insert useful argument description>
Type:
    Transformation
'''

SELECT_COLUMNS_DOC = ''' <Insert your function description here>
Example:
    x = on df | select columns $columns[$col]
Grammar:
    select columns $columns[$col]
Args:
    columns: <Insert useful argument description>
Type:
    Transformation
'''

SELECT_ROWS_DOC = ''' <Insert your function description here>
Example:
    x = on df | select rows !condition
Grammar:
    select rows !condition
Args:
    condition: <Insert useful argument description>
Type:
    Transformation
'''

DROP_COLUMNS_DOC = ''' <Insert your function description here>
Example:
    x = on df | drop columns $columns[$col]
Grammar:
    drop columns $columns[$col]
Args:
    columns: <Insert useful argument description>
Type:
    Transformation
'''

JOIN_DOC = ''' <Insert your function description here>
Example:
    x = on df | join $how $table on $columns[$col]
Grammar:
    join $how $table on $columns[$col]
    how := {left, right, outer, inner}
Args:
    how: <Insert useful argument description>
    table: <Insert useful argument description>
    columns: <Insert useful argument description>
Type:
    Transformation
'''

GROUP_BY_DOC = ''' <Insert your function description here>
Example:
    on df | group by $columns[$col] apply $aggregation
Grammar:
    group by $columns[$col] apply $aggregation
    aggregation := {min,max,sum,avg,mean,count}
Args:
    columns: <Insert useful argument description>
    aggregation: <Insert useful argument description>
Type:
    Operation
'''

REPLACE_VALUES_DOC = ''' <Insert your function description here>
Example:
    on df | replace $old_value with $new_value
Grammar:
    replace $old_value with $new_value
Args:
    old_value: <Insert useful argument description>
    new_value: <Insert useful argument description>
Type:
    Operation
'''

APPEND_DOC = ''' <Insert your function description here>
Example:
    x = on df | append column !col_expr as $col_name
Grammar:
    append column !col_expr as $col_name
Args:
    col_expr: <Insert useful argument description>
    col_name: <Insert useful argument description>
Type:
    Transformation
'''

SORT_BY_DOC = ''' <Insert your function description here>
Example:
    x = on df | sort by $columns[$col $order]
Grammar:
    sort by $columns[$col $order]
    order := {ascending, descending}
Args:
    columns: <Insert useful argument description>
Type:
    Transformation
'''

DROP_DUPLICATES_DOC = ''' <Insert your function description here>
Example:
    x = on df | drop duplicates
Grammar:
    drop duplicates
Args:
Type:
    Transformation
'''

RENAME_DOC = ''' <Insert your function description here>
Example:
    x = on df | rename columns $columns[$current to $new]
Grammar:
    rename columns $columns[$current to $new]
Args:
    columns: <Insert useful argument description>
Type:
    Transformation
'''

SHOW_DOC = ''' <Insert your function description here>
Example:
    on df | show
Grammar:
    show
Args:
Type:
    Operation
'''

DESCRIBE_DOC = ''' <Insert your function description here>
Example:
    on df | describe
Grammar:
    describe
Args:
Type:
    Operation
'''

COUNT_DOC = ''' <Insert your function description here>
Example:
    on df | count
Grammar:
    count
Args:
Type:
    Operation
'''

SHOW_SCHEMA_DOC = ''' <Insert your function description here>
Example:
    on df | show schema
Grammar:
    show schema
Args:
Type:
    Operation
'''

HEAD_DOC = ''' <Insert your function description here>
Example:
    on df | head $num_rows
Grammar:
    head $num_rows
Args:
    num_rows: <Insert useful argument description>
Type:
    Operation
'''

TEST_DSL_DOC = ''' <Insert your function description here>
Example:
    x = test $dataframe | transformer n ... | operation
Grammar:
    test $dataframe
Args:
    dataframe: <Insert useful argument description>
Type:
    Initialization
'''


