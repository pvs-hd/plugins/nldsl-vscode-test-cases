EXPRESSION_ONLY_DOC = ''' <Insert your function description here>
Example:
    Expression to be evaluated, e.g. x = z % (3 + a / p)
Grammar:
    !expr
Args:
    expr: <Insert useful argument description>
Type:
    Function
'''

ON_TURTLE_DOC = ''' Testing on the on_turtle function
Example:
    x = on $turtle | transformer n ... | operation
Grammar:
    on $turtle
Args:
    turtle: <Insert useful argument description>
Type:
    Initialization
'''

CREATE_TURTLE_DOC = ''' Create on turtle function
Example:
    x = create one turtle | transformer n ... | operation
Grammar:
    create one turtle
Args:
Type:
    Initialization
'''

SHOW_DOC = ''' <Insert your function description here>
Example:
    on df | show
Grammar:
    show
Args:
Type:
    Operation
'''

PENUP_DOC = ''' <Insert your function description here>
Example:
    on df | penup
Grammar:
    penup
Args:
Type:
    Operation
'''

PENDOWN_DOC = ''' <Insert your function description here>
Example:
    on df | pendown
Grammar:
    pendown
Args:
Type:
    Operation
'''

BEGIN_FILL_DOC = ''' <Insert your function description here>
Example:
    on df | begin_fill
Grammar:
    begin_fill
Args:
Type:
    Operation
'''

END_FILL_DOC = ''' <Insert your function description here>
Example:
    on df | end_fill
Grammar:
    end_fill
Args:
Type:
    Operation
'''

FORWARD_DOC = ''' <Insert your function description here>
Example:
    on df | forward $amount
Grammar:
    forward $amount
Args:
    amount: <Insert useful argument description>
Type:
    Operation
'''

BACKWARD_DOC = ''' <Insert your function description here>
Example:
    on df | backward $amount
Grammar:
    backward $amount
Args:
    amount: <Insert useful argument description>
Type:
    Operation
'''

RIGHT_DOC = ''' <Insert your function description here>
Example:
    on df | right $angle
Grammar:
    right $angle
Args:
    angle: <Insert useful argument description>
Type:
    Operation
'''

LEFT_DOC = ''' <Insert your function description here>
Example:
    on df | left $angle
Grammar:
    left $angle
Args:
    angle: <Insert useful argument description>
Type:
    Operation
'''

PENCOLOR_DOC = ''' <Insert your function description here>
Example:
    on df | pencolor $color
Grammar:
    pencolor $color
Args:
    color: <Insert useful argument description>
Type:
    Operation
'''

FILLCOLLOR_DOC = ''' <Insert your function description here>
Example:
    on df | fillcolor $color
Grammar:
    fillcolor $color
Args:
    color: <Insert useful argument description>
Type:
    Operation
'''

GOTO_DOC = ''' <Insert your function description here>
Example:
    on df | goto $x $y
Grammar:
    goto $x $y
Args:
    x: <Insert useful argument description>
    y: <Insert useful argument description>
Type:
    Operation
'''

DOT_DOC = ''' <Insert your function description here>
Example:
    on df | dot $size
Grammar:
    dot $size
Args:
    size: <Insert useful argument description>
Type:
    Operation
'''


