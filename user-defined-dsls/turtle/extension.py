import nldsl as nl
from nldsl.core import ExpressionRule, OperatorType
from . import docs

class TurtleCodeGenerator(nl.CodeGenerator):
    pass

class TurtleExpr(ExpressionRule):
    def __init__(self, expr_name, next_keyword):
        super().__init__(expr_name, next_keyword)
        # Create operator maps and types, e.g.:
        # self.operator_map['and'] = ' & '
        # self.operator_type['in'] = OperatorType.UNARY_FUNCTION
        self.operator_map['and'] = ' & '
        self.operator_map['or'] = ' | '
        self.operator_map['in'] = '.isin'
        self.operator_map['not'] = '~'
        self.operator_type['in'] = OperatorType.UNARY_FUNCTION

@nl.core.grammar(docs.EXPRESSION_ONLY_DOC)
def func_expression_only(code, args):
    return code + args["expr"]

TurtleCodeGenerator.register_function(func_expression_only)

@nl.core.grammar(docs.ON_TURTLE_DOC)
def func_on_turtle(code, args):
    return code + args["turtle"]

TurtleCodeGenerator.register_function(func_on_turtle)

@nl.core.grammar(docs.CREATE_TURTLE_DOC)
def func_create_turtle(code, env):
    return code + ".Turtle.Turtle()"

TurtleCodeGenerator.register_function(func_create_turtle)

@nl.core.grammar(docs.SHOW_DOC)
def func_show(code):
    return code + ".show()"

TurtleCodeGenerator.register_function(func_show)

@nl.core.grammar(docs.PENUP_DOC)
def func_penup(code):
    return code + ".penup()"

TurtleCodeGenerator.register_function(func_penup)

@nl.core.grammar(docs.PENDOWN_DOC)
def func_pendown(code):
    return code + ".pendown()"

TurtleCodeGenerator.register_function(func_pendown)

@nl.core.grammar(docs.BEGIN_FILL_DOC)
def func_begin_fill(code):
    return code + ".begin_fill()"

TurtleCodeGenerator.register_function(func_begin_fill)

@nl.core.grammar(docs.END_FILL_DOC)
def func_end_fill(code):
    return code + ".end_fill()"

TurtleCodeGenerator.register_function(func_end_fill)

@nl.core.grammar(docs.FORWARD_DOC)
def func_forward(code, args):
    return code + ".forward({0})".format(args["amount"])

TurtleCodeGenerator.register_function(func_forward)

@nl.core.grammar(docs.BACKWARD_DOC)
def func_backward(code, args):
    return code + ".backward({0})".format(args["amount"])

TurtleCodeGenerator.register_function(func_backward)

@nl.core.grammar(docs.RIGHT_DOC)
def func_right(code, args):
    return code + ".right({0})".format(args["angle"])

TurtleCodeGenerator.register_function(func_right)

@nl.core.grammar(docs.LEFT_DOC)
def func_left(code, args):
    return code + ".left({0})".format(args["angle"])

TurtleCodeGenerator.register_function(func_left)

@nl.core.grammar(docs.PENCOLOR_DOC)
def func_pencolor(code, args):
    return code + ".pencolor({0})".format(args["color"])

TurtleCodeGenerator.register_function(func_pencolor)

@nl.core.grammar(docs.FILLCOLLOR_DOC)
def func_fillcollor(code, args):
    return code + ".fillcolor({0})".format(args["color"])

TurtleCodeGenerator.register_function(func_fillcollor)

@nl.core.grammar(docs.GOTO_DOC)
def func_goto(code, args):
    return code + ".goto({0},{1})".format(args["x"], args["y"])

TurtleCodeGenerator.register_function(func_goto)

@nl.core.grammar(docs.DOT_DOC)
def func_dot(code, args):
    return code + ".dot({0})".format(args["size"])

TurtleCodeGenerator.register_function(func_dot)

CODE_GEN = TurtleCodeGenerator()

