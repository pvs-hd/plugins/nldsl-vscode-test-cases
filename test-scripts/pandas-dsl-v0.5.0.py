# Demo for NLDSL
# Dataset Covid_19
# (C) 2022 Kim Tuyen Le, Christopher Höllriegl


## target code = pandas 
import pandas as pd

#! name 'data', type 'csv', path 'data/covid_19_data.csv'
# Dataframe 'data' will have the following columns:
# SNo, ObservationDate, Province/State, Country/Region, Last Update, Confirmed, Deaths, Recovered

# Load the csv file with headers into dataframe 'data'
## data = load from 'data/covid_19_data.csv' as csv_with_header 
data = pd.read_csv('data/covid_19_data.csv') 

# Display a part of the dataframe
## on data | show 
print(data) 

# Grouping rows of the dataframe 'data' by the column 'Country/Region', summing 'Deaths' and then sorting by the column 'Last Update' descending
## on data | group by 'Country/Region', 'Last Update'  | apply sum on 'Deaths'  as 'DeathsSummed' | sort by 'Last Update' descending | show 
print(data.groupby(['Country/Region', 'Last Update']).agg({'Deaths' : 'sum'}).rename(columns={'Deaths' : 'DeathsSummed'}).sort_values(['Last Update'], axis='index', ascending=[False])) 

# Selecting columns 'Province/State', 'Country/Region' and 'Confirmed' of the dataframe 'data', and printing the first 10 rows
## on data | select columns 'Province/State' , 'Country/Region' , 'Confirmed' | head 10 | show 
print(data[['Province/State', 'Country/Region', 'Confirmed']].head(10)) 

# Appending to dataframe 'data' a new column 'Active' which is derived from 'Confirmed', 'Deaths' and 'Recovered'
## on data | append column data.Confirmed - data.Deaths - data.Recovered as 'Active' 
data.assign(**{'Active': data.apply(lambda row: row.Confirmed - row.Deaths - row.Recovered, axis=1).values}) 

# Selecting column 'Province/State' of the dataframe 'data'
## on data | select columns 'Last Update' | show 
print(data[['Last Update']]) 

# Select column 'Last Update' and print it
## on data | select columns 'Last Update' | show 
print(data[['Last Update']]) 

# Use describe on the dataset
## on data | describe | show 
print(data.describe()) 

# Show schema of the dataset
## on data | show schema 
data.info(verbose=False) 

# Sort the dataset by 'Confirmed' - ascending and print it
## on data | sort by 'Confirmed' ascending | show 
print(data.sort_values(['Confirmed'], axis='index', ascending=[True])) 

#$ div columns $col1 $col2 as $res = append column $col1 / $col2 as $res

# Prevent divide by zero error
data = data.drop(data[data.Confirmed == 0.0].index)

## on data | div columns data.Deaths data.Confirmed as 'deathsRatio' 
data.assign(**{'deathsRatio': data.apply(lambda row: row.Deaths / row.Confirmed, axis=1).values}) 

# Rename "Province/State" to "State"
## data = on data | rename columns 'Province/State' to 'State' 
data = data.rename(columns={'Province/State': 'State'}) 

# Group by state and use the aggregation sum on Deaths named as avgDeaths. Show the result.
## on data | group by 'State' | apply sum on 'Deaths' as 'avgDeaths' | show 
print(data.groupby(['State']).agg({'Deaths' : 'sum'}).rename(columns={'Deaths' : 'avgDeaths'})) 