# Demo for NLDSL
# Dataset Covid_19
# Kim Tuyen Le, July 2020

## target code = pandas, libs = plotting, plotting

import pandas as pd

#! name 'data', type 'csv', path 'data/covid_19_data.csv'
# Dataframe 'data' will have the following columns:
# SNo, ObservationDate, Province/State, Country/Region, Last Update, Confirmed, Deaths, Recovered

# Load the csv file into dataframe 'data'
## data = load from 'data/covid_19_data.csv' as csv 
data = pd.read_csv('data/covid_19_data.csv') 

# Grouping rows of the dataframe 'data' by the column 'Country/Region', summing and then sorting by 'Confirmed'
## on data | group by 'Country/Region' apply sum | sort by 'Confirmed' descending | show 
print(data.groupby(['Country/Region']).sum().sort_values(['Confirmed'], axis='index', ascending=[False])) 

# Selecting columns 'Country/Region' and 'Confirmed' of the dataframe 'data', and printing the first 10 rows
## on data | select columns 'Country/Region', 'Confirmed' | head 10 | show 
print(data[['Country/Region', 'Confirmed']].head(10))

# Appending to dataframe 'data' a new column 'Active' which is derived from 'Confirmed', 'Deaths' and 'Recovered'
## on data | append column data.Confirmed - data.Deaths - data.Recovered as 'Active' 
data.assign(**{'Active': data.apply(lambda row: row.Confirmed - row.Deaths - row.Recovered, axis=1).values}) 

# Selecting column 'Province/State' of the dataframe 'data'
## on data | select columns 'Province/State' 
data[['Province/State']] 

## on data | select columns 'Last Update' | show 
print(data[['Last Update']]) 

## on data | describe | show 
print(data.describe()) 
## on data | show schema 
data.info(verbose=False) 

#$ div columns $col1 $col2 as $res = append column $col1 / $col2 as $res

## on data | div columns data.Deaths data.Confirmed as 'deathsRatio' 
data.assign(**{'deathsRatio': data.apply(lambda row: row.Deaths / row.Confirmed, axis=1).values}) 