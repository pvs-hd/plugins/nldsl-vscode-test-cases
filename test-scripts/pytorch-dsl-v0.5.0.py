## target code = pytorch 
import torch as pyTorch
import torchvision


## mnist_train = load sample train data mnist 
mnist_train = torchvision.datasets.MNIST('./data', download=True, train=True, transform=None) 

## mnist_train = load sample train data mnist | transform toTensor yes Normalize "0.1307" "0.3081" 
mnist_train = torchvision.datasets.MNIST('./data', download=True, train=True, transform=torchvision.transforms.Compose([torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((0.1307,), (0.3081,))])) 

## mnist_test = load sample test data mnist 
mnist_test = torchvision.datasets.MNIST('./data', download=True, train=False, transform=None) 

## mnist_test = load sample test data mnist | transform toTensor yes Normalize "0.1307" "0.3081" 
mnist_test = torchvision.datasets.MNIST('./data', download=True, train=False, transform=torchvision.transforms.Compose([torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((0.1307,), (0.3081,))])) 

## create myModel 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()

    def forward(self, x):
        return x 

## create myModel | model type feed_forward 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()

    def forward(self, x):
        return x 

## create model myModel | model type feed_forward | add layer convolutional 1 32 3 relu 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = pyTorch.nn.Conv2d(1, 32, 3)

    def forward(self, x):
        x = self.conv1(x)
        x = pyTorch.nn.functional.relu(x)
        return x 

## create model myModel | model type feed_forward | add layer convolutional 1 32 3 relu | add layer convolutional 32 64 3 relu 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = pyTorch.nn.Conv2d(1, 32, 3)
        self.conv2 = pyTorch.nn.Conv2d(32, 64, 3)

    def forward(self, x):
        x = self.conv1(x)
        x = pyTorch.nn.functional.relu(x)
        x = self.conv2(x)
        x = pyTorch.nn.functional.relu(x)
        return x 

## create model myModel | model type feed_forward | add layer convolutional 1 32 3 relu | add layer convolutional 32 64 3 relu | add layer flatten 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = pyTorch.nn.Conv2d(1, 32, 3)
        self.conv2 = pyTorch.nn.Conv2d(32, 64, 3)

    def forward(self, x):
        x = self.conv1(x)
        x = pyTorch.nn.functional.relu(x)
        x = self.conv2(x)
        x = pyTorch.nn.functional.relu(x)
        x = pyTorch.flatten(x, 1)
        return x 

## create model myModel | model type feed_forward | add layer convolutional 1 32 3 relu | add layer convolutional 32 64 3 relu | add layer flatten | add layer fully connected 36864 128 relu 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = pyTorch.nn.Conv2d(1, 32, 3)
        self.conv2 = pyTorch.nn.Conv2d(32, 64, 3)
        self.fc3 = pyTorch.nn.Linear(36864, 128)

    def forward(self, x):
        x = self.conv1(x)
        x = pyTorch.nn.functional.relu(x)
        x = self.conv2(x)
        x = pyTorch.nn.functional.relu(x)
        x = pyTorch.flatten(x, 1)
        x = self.fc3(x)
        x = pyTorch.nn.functional.relu(x)
        return x 

## create model myModel | model type feed_forward | add layer convolutional 1 32 3 relu | add layer convolutional 32 64 3 relu | add layer flatten | add layer fully_connected 36864 128 relu | add layer fully_connected 128 10 softmax 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = pyTorch.nn.Conv2d(1, 32, 3)
        self.conv2 = pyTorch.nn.Conv2d(32, 64, 3)
        self.fc3 = pyTorch.nn.Linear(36864, 128)
        self.fc4 = pyTorch.nn.Linear(128, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = pyTorch.nn.functional.relu(x)
        x = self.conv2(x)
        x = pyTorch.nn.functional.relu(x)
        x = pyTorch.flatten(x, 1)
        x = self.fc3(x)
        x = pyTorch.nn.functional.relu(x)
        x = self.fc4(x)
        x = pyTorch.nn.functional.softmax(x)
        return x  

## on myModel | compile 
myModel = MyModel()

## on myModel | compile | optimizer adam 
myModel = MyModel()
_optimizer = pyTorch.optim.Adam(myModel.parameters())

## on myModel | compile | optimizer sgd 
myModel = MyModel()
_optimizer = pyTorch.optim.SGD(myModel.parameters())
 
## on myModel | compile | optimizer just_a_wrong_opt 
myModel = MyModel()
_optimizer = pyTorch.optim.just_a_wrong_opt(myModel.parameters())

## on myModel | compile | optimizer adam | loss sparse_categorical_crossentropy 
myModel = MyModel()
_optimizer = pyTorch.optim.Adam(myModel.parameters())
_loss_fn = pyTorch.nn.functional.cross_entropy

## on myModel | train 
def train(model, train_loader, optimizer, loss_fn, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()

_epochs = 1
train_batch_size = 64
_train_loader = pyTorch.utils.data.DataLoader(__data__, batch_size=train_batch_size, shuffle=True)
for epoch in range(1, _epochs + 1):
    train(myModel, _train_loader, _optimizer, _loss_fn, epoch) 

## on myModel | train | using mnist_train data with labels 
def train(model, train_loader, optimizer, loss_fn, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()

_epochs = 1
train_batch_size = 64
_train_loader = pyTorch.utils.data.DataLoader(mnist_train, batch_size=train_batch_size, shuffle=True)
for epoch in range(1, _epochs + 1):
    train(myModel, _train_loader, _optimizer, _loss_fn, epoch) 

## on myModel | train | using mnist_train data with labels | epochs 10 
def train(model, train_loader, optimizer, loss_fn, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()

_epochs = 10
train_batch_size = 64
_train_loader = pyTorch.utils.data.DataLoader(mnist_train, batch_size=train_batch_size, shuffle=True)
for epoch in range(1, _epochs + 1):
    train(myModel, _train_loader, _optimizer, _loss_fn, epoch) 

## on myModel | train | using mnist_train data with labels | epochs 5 | batch size 128 
def train(model, train_loader, optimizer, loss_fn, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()

_epochs = 5
train_batch_size = 128
_train_loader = pyTorch.utils.data.DataLoader(mnist_train, batch_size=train_batch_size, shuffle=True)
for epoch in range(1, _epochs + 1):
    train(myModel, _train_loader, _optimizer, _loss_fn, epoch) 

## on myModel | evaluate 
def test(model, test_loader, loss_fn):
    model.eval()
    test_loss = 0
    correct = 0
    with pyTorch.no_grad():
        for data, target in test_loader:
            output = model(data)
            test_loss += loss_fn(output, target, reduction='sum').item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)

test_batch_size = 64
_test_loader = pyTorch.utils.data.DataLoader(__data__, batch_size=test_batch_size, shuffle=True)
test(myModel, _test_loader, _loss_fn) 

## on myModel | evaluate | using mnist_test data with labels 
def test(model, test_loader, loss_fn):
    model.eval()
    test_loss = 0
    correct = 0
    with pyTorch.no_grad():
        for data, target in test_loader:
            output = model(data)
            test_loss += loss_fn(output, target, reduction='sum').item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)

test_batch_size = 64
_test_loader = pyTorch.utils.data.DataLoader(mnist_test, batch_size=test_batch_size, shuffle=True)
test(myModel, _test_loader, _loss_fn) 

## on myModel | evaluate | using mnist_test data with labels | batch size 128 
def test(model, test_loader, loss_fn):
    model.eval()
    test_loss = 0
    correct = 0
    with pyTorch.no_grad():
        for data, target in test_loader:
            output = model(data)
            test_loss += loss_fn(output, target, reduction='sum').item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)

test_batch_size = 128
_test_loader = pyTorch.utils.data.DataLoader(mnist_test, batch_size=test_batch_size, shuffle=True)
test(myModel, _test_loader, _loss_fn) 

## create model 
class MyModel(pyTorch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()

    def forward(self, x):
        return x 