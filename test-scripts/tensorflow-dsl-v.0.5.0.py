# Demo for NLDSL
# Dataset Covid_19
# (C) 2022 Kim Tuyen Le, Christopher Höllriegl
# Ref: https://gitlab.com/pvs-hd/DSLs/dsl-deeplearning/-/blob/master/workspace/mnist/mnist-for-tensorflow.dsl.py

## target code = tensorflow 
import tensorflow as tf


## mnist_train = load sample train data mnist 
mnist_train = (tf.keras.datasets.mnist.load_data()[0][0], tf.keras.datasets.mnist.load_data()[0][1]) 

## mnist_train = load sample train data mnist | normalize between zero and 1 lower bound 0 upper bound 255 
mnist_train = ((tf.keras.datasets.mnist.load_data()[0][0] - 0) / (255 - 0), tf.keras.datasets.mnist.load_data()[0][1]) 

## mnist_test = load sample test data mnist | normalize between zero and 1 lower bound 0 upper bound 255 
mnist_test = ((tf.keras.datasets.mnist.load_data()[1][0] - 0) / (255 - 0), tf.keras.datasets.mnist.load_data()[1][1]) 

## create model myModel 
myModel = tf.keras.models 

## create model myModel | model type feed_forward 
myModel = tf.keras.models.Sequential([
    ]) 

## create model myModel | model type feed_forward | add layer flatten | add layer fully connected 36864 128 none 
myModel = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(), 
    tf.keras.layers.Dense(128)
    ]) 

## create model myModel | model type feed_forward | add layer flatten | add layer fully connected 36864 128 relu | add layer dropout probability 20 | add layer fully connected 128 10 none 
myModel = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(), 
    tf.keras.layers.Dense(128, activation='relu'), 
    tf.keras.layers.Dropout(0.2), 
    tf.keras.layers.Dense(10)
    ]) 

myLossFn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

## on myModel | compile 
myModel.compile() 

## on myModel | compile | optimizer adam 
myModel.compile(optimizer='adam') 

## on myModel | compile | optimizer just_unknow_opt 
myModel.compile(optimizer='just_unknow_opt') 

## on myModel | compile | optimizer adam | loss myLossFn 
myModel.compile(optimizer='adam', loss=myLossFn) 

## on myModel | compile | optimizer adam | loss myLossFn | metrics accuracy 
myModel.compile(optimizer='adam', loss=myLossFn, metrics=['accuracy']) 

## on myModel | train 
myModel.fit() 

## on myModel | train | using mnist_train data with labels 
myModel.fit(mnist_train[0], mnist_train[1]) 

## on myModel | train | using mnist_train data with labels | epochs 10 
myModel.fit(mnist_train[0], mnist_train[1], epochs=10) 

## on myModel | evaluate 
myModel.evaluate() 

## on myModel | evaluate | using mnist_test data with labels 
myModel.evaluate(mnist_test[0], mnist_test[1]) 

## on myModel | evaluate | using mnist_test data with labels | verbose 2 
myModel.evaluate(mnist_test[0], mnist_test[1], verbose=2) 

## on myModel | predict 
myModel.predict() 

## on myModel | predict | using mnist_test data without labels 
myModel.predict(mnist_test[0]) 

## on myModel | predict | using mnist_test data without labels | batch size 128 
myModel.predict(mnist_test[0], batch_size=128)

## on myModel | predict | using mnist_test data with labels 
myModel.predict(mnist_test[0], mnist_test[1]) 

## x = create model mymodel 
x = mymodel = tf.keras.models 

## create custom layer myLayer example 
class myLayer(tf.keras.layers.Layer):
    def __init__(self, units=32):
        super(myLayer, self).__init__()
        self.units = units

    def build(self, input_shape):
        self.w = self.add_weight(shape=(input_shape[-1], self.units),initializer='random_normal',trainable=True)
        self.b = self.add_weight(shape=(self.units,),initializer='random_normal',trainable=True)

    def call(self, inputs):		return tf.matmul(inputs, self.w) + self.b 
