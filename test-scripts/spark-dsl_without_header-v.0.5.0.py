# Demo for NLDSL
# Dataset Covid_19
# (C) 2022 Kim Tuyen Le, Christopher Höllriegl

## target code = spark  
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName("Spark Example").getOrCreate()

#! name 'data', type 'csv', path 'data/covid_19_data_no_header.csv'
# Dataframe 'data' will have the following columns and their respective names with the no header file:
# SNo = _c0
# ObservationDate = _c1
# Province/State = _c2
# Country/Region = _c3
# Last Update = _c4
# Confirmed = _c5
# Deaths = _c6
# Recovered  = _c7

# Load the csv file without headers into the dataframe 'data'
## data = load from 'data/covid_19_data_no_header.csv' as csv_without_header 
data = spark.read.format('csv').load('data/covid_19_data_no_header.csv') 

# Display a part of the dataframe
## on data | show 
data.show() 

# Rename columns
## data = on data | rename columns '_c3' to 'Country/Region', '_c4' to 'Last Update', '_c6' to 'Deaths', '_c2' to 'Province/State', '_c5' to 'Confirmed', '_c7' to 'Recovered' 
data = data.withColumnRenamed('_c3', 'Country/Region').withColumnRenamed('_c4', 'Last Update').withColumnRenamed('_c6', 'Deaths').withColumnRenamed('_c2', 'Province/State').withColumnRenamed('_c5', 'Confirmed').withColumnRenamed('_c7', 'Recovered') 

# Grouping rows of the dataframe 'data' by the column 'Country/Region', summing 'Deaths' and then sorting by the column 'Last Update' descencing
## on data | group by 'Country/Region', 'Last Update'  | apply sum on 'Deaths'  as 'DeathsSummed' | sort by 'Last Update' descending | show 
data.groupBy(['Country/Region', 'Last Update']).agg(sum('Deaths').alias('DeathsSummed')).sort(['Last Update'], ascending=[False]).show() 

# Selecting columns 'Province/State', 'Country/Region' and 'Confirmed' of the dataframe 'data', and printing the first 10 rows
## on data | select columns 'Province/State' , 'Country/Region' , 'Confirmed' | head 10 | show 
data.select(['Province/State', 'Country/Region', 'Confirmed']).show(10) 

# Appending to dataframe 'data' a new column 'Active' which is derived from 'Confirmed', 'Deaths' and 'Recovered'
## on data | append column data.Confirmed - data.Deaths - data.Recovered as 'Active' 
data.withColumn('Active', data.Confirmed - data.Deaths - data.Recovered) 

# Selecting column 'Province/State' of the dataframe 'data'
## on data | select columns 'Province/State' 
data.select(['Province/State']) 

# Select column 'Last Update' and print it
## on data | select columns 'Last Update' | show 
data.select(['Last Update']).show() 

# Use describe on the dataset
## on data | describe | show 
data.describe().show() 

# Show schema of the dataset
## on data | show schema 
data.printSchema() 

# Sort the dataset by 'Confirmed' - ascending and print it
## on data | sort by 'Confirmed' ascending | show 
data.sort(['Confirmed'], ascending=[True]).show() 

#$ div columns $col1 $col2 as $res = append column $col1 / $col2 as $res

## on data | div columns data.Deaths data.Confirmed as 'deathsRatio' 
data.withColumn('deathsRatio', data.Deaths / data.Confirmed) 

# Rename "Province/State" to "State"
## data = on data | rename columns 'Province/State' to 'State' 
data = data.withColumnRenamed('Province/State', 'State') 

# Group by state and use the aggregation sum on Deaths named as avgDeaths. Show the result.
## on data | group by 'State' | apply sum on 'Deaths' as 'avgDeaths' | show 
data.groupBy(['State']).agg(sum('Deaths').alias('avgDeaths')).show() 